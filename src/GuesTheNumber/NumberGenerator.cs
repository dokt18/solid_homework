﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GuesTheNumber
{
    internal class NumberGenerator : IGenerator<int>
    {
        public int Generate(IEnumerable listForGame)
        {
            var list = listForGame.Cast<int>().ToList();
            Random rnd = new Random();
            int randomIndex = rnd.Next(list.Count);
            return list[randomIndex];
        }
    }
}
