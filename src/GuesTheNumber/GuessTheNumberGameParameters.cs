﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;

namespace GuesTheNumber
{
    internal class GuessTheNumberGameParameters: IGuessTheNumberParameters
    {
        private int retry { get; set; }
        private int number { get; set; }
        private List<int> fromList { get; set; }

        public IGuessTheNumberParameters CreateParameters(int _retry, int _number, IEnumerable<int> fromList)
        {
            var parameters = new GuessTheNumberGameParameters() { retry = _retry, number = _number, fromList = fromList.ToList() };
            return parameters;
        }

        public int GetRetry()
        {
            return retry;
        }

        public int GetNumber()
        {
            return number;
        }
        public IEnumerable<int> GetFromList()
        {
            return fromList;
        }
        public IParameters GetParameters()
        {
            return (IParameters)this;
        }

        public IParameters GetParameters()
        {
            throw new NotImplementedException();
        }
    }
}
