﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuesTheNumber
{
    internal interface IGuessTheNumberParameters: IParameters
    {
        public IGuessTheNumberParameters CreateParameters(int _retry, int _number, IEnumerable<int> fromList);
        public int GetNumber();
        public int GetRetry();
        public IEnumerable<int> GetFromList();
    }
}
