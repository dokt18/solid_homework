﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace GuesTheNumber
{
    internal interface IGenerator<T>
    {
        public T Generate(IEnumerable listForGame);
    }
}
