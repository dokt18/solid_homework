﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuesTheNumber
{
    internal class GuessTheNumberGame : IGame
    {
        public void Play(IGuessTheNumberParameters gameParameters)
        {
            Console.WriteLine(gameParameters.GetNumber().ToString() + gameParameters.GetRetry().ToString());
            Console.WriteLine("I made a number from this list: ");
            foreach(var n in gameParameters.GetFromList())
            {
                Console.Write(n.ToString() + ", ");
            }
            Console.WriteLine("\nyou have to guess it.\n");
            int attemptsLeft = gameParameters.GetRetry();
            for (int i = 0; i < gameParameters.GetRetry(); i++)
            {
                int answer;
                Console.WriteLine("You have attempts left: " + attemptsLeft + ".\n");
                Console.WriteLine("Enter the number:");

                string input = Console.ReadLine();
                if (int.TryParse(input, out answer))
                {
                    if(answer == gameParameters.GetNumber())
                    {
                        Console.WriteLine("You are right!");
                        break;
                    } else
                    {
                        Console.WriteLine("Wrong!");
                    }
                }
                else
                {
                    Console.WriteLine("You have entered not a number! Minus one attempt!.\n");
                }
                attemptsLeft--;
            }
        }
    }
}
