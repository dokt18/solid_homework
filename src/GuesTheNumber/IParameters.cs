﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuesTheNumber
{
    internal interface IParameters
    {
        public IParameters GetParameters();
    }
}
