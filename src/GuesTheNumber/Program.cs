﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Authentication.ExtendedProtection;
using System.Threading.Tasks;

namespace GuesTheNumber
{
    internal class Program
    {
        public static IConfigurationRoot configuration;
        public static IGenerator<int> numberGenerator;
        public static IGuessTheNumberParameters gameParameters;
        public static IGame game;
        static void Main(string[] args)
        {
            var services = new ServiceCollection()
                .AddSingleton<IConfigurationRoot>(GetConfiguration())
                .AddSingleton<IGenerator<int>, NumberGenerator>()
                .AddSingleton<IGuessTheNumberParameters, GuessTheNumberGameParameters>()
                .AddSingleton<IGame, GuessTheNumberGame>()
                .BuildServiceProvider();

            configuration = services.GetService<IConfigurationRoot>();
            numberGenerator = services.GetService<IGenerator<int>>();
            gameParameters = services.GetService<IGuessTheNumberParameters>();
            game = services.GetService<IGame>();

            game.Play(gameParameters.CreateParameters(
                configuration.GetValue<int>("retry"),
                numberGenerator.Generate(configuration.GetSection("listForGame").Get<List<int>>()),
                configuration.GetSection("listForGame").Get<List<int>>()));
        }

        static IConfigurationRoot GetConfiguration()
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json", optional: false)
                .Build();

            return configuration;
        } 
    }
}
