﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuesTheNumber
{
    internal interface IGame
    {
        public void Play(IGuessTheNumberParameters gameParameters);
    }
}
